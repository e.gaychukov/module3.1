﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
            Task1 task1 = new Task1();
            Console.WriteLine(task1.Multiplication(-2, 0));
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if(int.TryParse(source, out int result))
            {
                return result;
            }
            throw new ArgumentException("You have inputted incorrect value!");
        }

        public int Multiplication(int num1, int num2)
        {
            int product = 0; 
            for(int i = 0; i < Math.Abs(num1); i++)
            {
                product += Math.Abs(num2);
            }

            return (num1 > 0 ^ num2 > 0) ? -product : product;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            return int.TryParse(input, out result) && result >= 0;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            if (TryParseNaturalNumber(naturalNumber.ToString(), out naturalNumber))
            {
                List<int> list = new List<int>();
                for (int i = 0; i < naturalNumber; i++)
                {
                    list.Add(2 * i);
                    Console.WriteLine(2*i);
                }
                return list;
            }
            return GetEvenNumbers(int.Parse(Console.ReadLine()));
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            return int.TryParse(input, out result) && result >= 0;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            if (TryParseNaturalNumber(source.ToString(), out source))
            {
                return source.ToString().Replace(digitToRemove.ToString(), "");
            }
            return RemoveDigitFromNumber(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
        }
    }
}
